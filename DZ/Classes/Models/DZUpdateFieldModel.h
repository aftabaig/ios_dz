//
//  DZUpdateFieldModel.h
//  DZ
//
//  Created by Ikarma Khan on 15/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DZUpdateFieldModel : NSObject


@property (nonatomic) NSInteger fieldID;
@property (nonatomic) NSInteger sheetID;
@property (nonatomic) NSInteger subSheetID;
@property (nonatomic) NSString* date;
@property (nonatomic) NSString* time;



@end
