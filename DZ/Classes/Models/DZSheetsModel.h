//
//  DZSheetsModel.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DZSheetsModel : NSObject
/*
 "id": 1,
 "name": "Cutting Gauge Sheet",
 "lease": 1,
 "has_sub_sheets": false,
 "createdAt": "2014-05-01T19:13:01.320Z",
 "updatedAt": "2014-05-01T19:13:01.320Z"
 */

@property (nonatomic) NSInteger sheetsID;
@property (nonatomic) NSInteger leaseID;
@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString *createdAt;
@property (nonatomic,strong) NSString *updatedAt;
@property (nonatomic) BOOL hasSubSheets;

- (id) initWithDictionary:(NSDictionary*)dictSheets;

@end
