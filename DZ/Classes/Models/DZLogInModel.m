//
//  DZLogInModel.m
//  DZ
//
//  Created by Ikarma Khan on 23/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZLogInModel.h"

@implementation DZLogInModel

- (id) initWithDictionary:(NSDictionary *)userInfo{
    
    
    self = [super init];
    self.userName = [userInfo objectForKey:@"user_name"];
    self.apiKey = [userInfo objectForKey:@"api_key"];
    return self;
}


@end
