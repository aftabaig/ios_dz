//
//  DZFieldsModel.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DZFieldsModel : NSObject
/*
@property (nonatomic,strong) NSString *createdAt;
@property (nonatomic,strong) NSString *updatedAt;
@property (nonatomic,strong) NSString *dataType;
@property (nonatomic,strong) NSString *formula;
@property (nonatomic,strong) NSString *name;
@property (nonatomic) NSInteger minValue;
@property (nonatomic) NSInteger maxValue;
*/

@property (nonatomic) NSInteger fieldsID;
@property (nonatomic) NSInteger sheetID;
@property (nonatomic) NSInteger subSheetID;
@property (nonatomic) NSInteger day;
@property (nonatomic) NSInteger month;
@property (nonatomic) NSInteger year;
@property (nonatomic,strong) NSArray *fieldsInfo;


- (id)initWithDictionary:(NSDictionary *)dictField;

@end
