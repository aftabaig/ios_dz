//
//  DZSubSheetsModel.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZSubSheetsModel.h"

@implementation DZSubSheetsModel

- (id) initWithDictionary:(NSDictionary *)dictSubSheets{
    
    self = [super init];
    self.subSheetsID = [[dictSubSheets objectForKey:@"id"]integerValue];
    self.sheetID = [[dictSubSheets objectForKey:@"sheet"]integerValue];
    self.name = [dictSubSheets objectForKey:@"name"];
    self.createdAt = [dictSubSheets objectForKey:@"createdAt"];
    self.updatedAt = [dictSubSheets objectForKey:@"updatedAt"];
    
    return self;
}

@end
