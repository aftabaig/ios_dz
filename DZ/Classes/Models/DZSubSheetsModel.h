//
//  DZSubSheetsModel.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DZSubSheetsModel : NSObject

/*
"id": 1,
"name": "Tank-1",
"sheet": 4,
"createdAt": "2014-04-28T06:31:19.227Z",
"updatedAt": "2014-04-28T06:31:19.227Z"
 
 */

@property (nonatomic) NSInteger subSheetsID;
@property (nonatomic) NSInteger sheetID;
@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* createdAt;
@property (nonatomic,strong) NSString *updatedAt;


- (id)initWithDictionary:(NSDictionary*)dictSubSheets;

@end
