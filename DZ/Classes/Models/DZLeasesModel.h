//
//  DZLeaseModel.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DZLeasesModel : NSObject
/*
 "id": 1,
 "name": "Cutting Lease",
 "createdAt": "2014-05-01T19:12:50.512Z",
 "updatedAt": "2014-05-01T19:12:50.512Z"
 */

@property (nonatomic) NSInteger leaseID;
@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* createdAt;
@property (nonatomic,strong) NSString* updatedAt;


- (id)initWithDictionary:(NSDictionary *)dictLease;

@end
