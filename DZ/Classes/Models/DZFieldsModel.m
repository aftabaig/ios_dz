//
//  DZFieldsModel.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZFieldsModel.h"

@implementation DZFieldsModel


- (id) initWithDictionary:(NSDictionary *)dictField{
    
    self = [super init];
    
    self.fieldsID = [[dictField objectForKey:@"id"]integerValue];

    NSDictionary *dateDict = [dictField objectForKey:@"date"];
    
    self.day = [[dateDict objectForKey:@"day"]integerValue];
    self.month =[[dateDict objectForKey:@"month"]integerValue];
    self.year = [[dateDict objectForKey:@"year"]integerValue];
    self.fieldsInfo = [dictField objectForKey:@"fields"];
    self.sheetID = [[dictField objectForKey:@"sheet"]integerValue];
    
    if([[dictField objectForKeyNotNull:@"sub_sheet"] isEqualToString:@""]){
        self.subSheetID = -1;
    }
    else
        self.subSheetID = [[dictField objectForKey:@"sub_sheet"]integerValue];
  
    // self.minValue = [[dictField objectForKey:@"main_value"]integerValue];
    // self.maxValue = [[dictField objectForKey:@"max_value"]integerValue];
  //  self.name = [dictField objectForKey:@"name"];
  //  self.createdAt = [dictField objectForKey:@"createdAt"];
  //  self.updatedAt = [dictField objectForKey:@"updatedAt"];
  //  self.dataType = [dictField objectForKey:@"data_type"];
  //  self.formula = [dictField objectForKey:@"formula"];
    
    
    
    return self;
}

@end
