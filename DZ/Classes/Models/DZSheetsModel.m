//
//  DZSheetsModel.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZSheetsModel.h"

@implementation DZSheetsModel


- (id)initWithDictionary:(NSDictionary *)dictSheets{
    
    self = [super init];
    
    self.sheetsID = [[dictSheets objectForKey:@"id"]integerValue];
    self.leaseID = [[dictSheets objectForKey:@"lease"]integerValue];
    self.name = [dictSheets objectForKey:@"name"];
    self.createdAt = [dictSheets objectForKey:@"createdAt"];
    self.updatedAt = [dictSheets objectForKey:@"updatedAt"];
    self.hasSubSheets = [[dictSheets objectForKey:@"has_sub_sheets"]boolValue];
    
    return self;
}

@end
