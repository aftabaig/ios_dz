//
//  DZLogInModel.h
//  DZ
//
//  Created by Ikarma Khan on 23/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DZLogInModel : NSObject

@property (nonatomic,strong) NSString *userName;
@property (nonatomic,strong) NSString *apiKey;

- (id)initWithDictionary:(NSDictionary*)userInfo;

@end
