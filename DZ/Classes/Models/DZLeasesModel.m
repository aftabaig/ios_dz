//
//  DZLeaseModel.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZLeasesModel.h"

@implementation DZLeasesModel

-(id)initWithDictionary:(NSDictionary *)dictLease{
    
    self = [super init];
    
    self.leaseID = [[dictLease objectForKey:@"id"]integerValue];
    self.name = [dictLease objectForKey:@"name"];
    self.createdAt = [dictLease objectForKey:@"createdAt"];
    self.updatedAt = [dictLease objectForKey:@"updatedAt"];
    
    return  self;
    
}

@end
