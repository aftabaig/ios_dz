//
//  DZAppDelegate.h
//  DZ
//
//  Created by Ikarma Khan on 28/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
