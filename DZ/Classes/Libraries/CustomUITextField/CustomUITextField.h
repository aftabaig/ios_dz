//
//  CustomUITextField.h
//  DZ
//
//  Created by Ikarma Khan on 03/06/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUITextField : UITextField

@property (nonatomic) float maxValue;
@property (nonatomic) float minValue;
@property (nonatomic) float value;
@end
