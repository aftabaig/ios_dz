//
//  NSDictionary+Utility.m
//  FastTaxi
//
//  Created by Beniamin Boariu on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSDictionary+Utility.h"

@implementation NSDictionary (Utility)

// in case of [NSNull null] values a nil is returned ...
- (id)objectForKeyNotNull:(id)key
{
    id object = [self objectForKey:key];
    
    if (object == [NSNull null] || object == nil)
        return @"";
    
    return object;
}

// if objectForKey returns nil, it will
// return an empty string.
- (id)objectForKeyWithEmptyString:(id)key
{
    id object = [self objectForKeyNotNull:key];
    if (object == nil)
    {
        return @"";
    }
    return object;
}


@end
