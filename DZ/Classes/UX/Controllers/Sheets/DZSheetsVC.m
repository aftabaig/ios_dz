//
//  DZSheetsViewController.m
//  DZ
//
//  Created by Ikarma Khan on 30/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#define FONT_SIZE 15.0f
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 10.0f

#import "DZSheetsVC.h"

#import "DZSubSheetsVC.h"
#import "DZFieldsVC.h"

#import "DZSheetsModel.h"
#import "DZSheetsManager.h"

#import "ZAActivityBar.h"

@interface DZSheetsVC () <DZSheetsManagerDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSArray *sheets;

@end

@implementation DZSheetsVC

@synthesize leaseID;

#pragma mark- -vielwLifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(initialze) forControlEvents:UIControlEventValueChanged];
    
    [self.refreshControl beginRefreshing];
    
  //  self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.4627 green:0.745 blue:0.3411 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    self.view.userInteractionEnabled = FALSE;
    
    [ZAActivityBar showWithStatus:@"Loading Sheets..."];
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
    
    [self initialze];
}
- (void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
}
- (void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
}

#pragma mark- -LeaseManager

- (void) initialze{
    
    DZSheetsManager *sheetManager = [[DZSheetsManager alloc]init];
    sheetManager.delegate = self;
    sheetManager.leaseID = self.leaseID;
    [sheetManager requestSheets];
}

#pragma mark- -SheetsManagerDelegate

- (void)sheetManager:(DZSheetsManager *)manager didFailToReceiveSheets:(NSError *)error{
    
    [self showErrorAlert:error];
    
}
- (void) sheetManager:(DZSheetsManager *)manager didReceiveSheets:(NSArray *)sheets error:(NSError*)error{
    
    [self.refreshControl endRefreshing];

    if(error != nil){
        [self showErrorAlert:error];
    }
    else
    {
        self.sheets = sheets;

        [self.tableView reloadData];
    }
}

- (void)showErrorAlert:(NSError*)error{
    
    [ZAActivityBar dismiss];
    [self.refreshControl endRefreshing];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Operation Failed" message:error.localizedDescription delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again", nil];
    
    [alert show];
    
}

#pragma mark- -TableViewDaraSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.sheets count];
}

-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DZSheetsModel *sheet = [self.sheets objectAtIndex:indexPath.row];
    
    //Get a reference to your string to base the cell size on.
    NSString *cellText = sheet.name;
    
    //set the desired size of your textbox
    CGSize constraint = CGSizeMake(320.0, MAXFLOAT);
    
    //set your text attribute dictionary
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
    
    //get the size of the text box
    CGRect textsize = [cellText boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    //calculate your size
    float textHeight = textsize.size.height +20;
    
    //I have mine set for a minimum size
    textHeight = (textHeight < 44.0) ? 44 : textHeight;
    
    //    NSLog(@"%f",textHeight);
    return textHeight;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    
    DZSheetsModel *sheetsModel = [self.sheets objectAtIndex:indexPath.row];
	NSString *cellName = sheetsModel.name;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = cellName;
    
    
    // cell.textLabel.textAlignment = NSTextAlignmentCenter;
    [[cell textLabel] setFont:[UIFont fontWithName:kAppFont size: 15.0]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [cell.textLabel setNumberOfLines:0];
    
    [cell.textLabel sizeToFit];
    return cell;
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    return view;
}

#pragma mark- -TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DZSheetsModel *sheetsModel = [self.sheets objectAtIndex:indexPath.row];
    
    if(sheetsModel.hasSubSheets){
        
        [self performSegueWithIdentifier:@"sheetsToSubSheetsView" sender:[NSNumber numberWithInt:sheetsModel.sheetsID]];
    }
    else{
        
        [self performSegueWithIdentifier:@"sheetsToFieldsView" sender:[NSNumber numberWithInt:sheetsModel.sheetsID]];
    }
    
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        [ZAActivityBar dismiss];
        
        [self.navigationItem setHidesBackButton:NO animated:NO];
        self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
        
        
        self.view.userInteractionEnabled = TRUE;
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    int sheetsID = [sender integerValue];
    
    if([segue.identifier isEqualToString:@"sheetsToSubSheetsView"]){
        
        DZSubSheetsVC *subSheetView = [segue destinationViewController];
        subSheetView.sheetsID = sheetsID;
        subSheetView.leaseID = self.leaseID;
    }
    else{
        
        DZFieldsVC *fieldsView = [segue destinationViewController];
        fieldsView.isSubSheetField = FALSE;
        fieldsView.sheetID = sheetsID;
        fieldsView.leaseID = self.leaseID;
    }
}

#pragma mark- -UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSLog(@"button index: %d",buttonIndex);
    if(buttonIndex == 1){
        [self initialze];
    }
    else if (buttonIndex ==0)
        [ZAActivityBar dismiss];
}

@end
