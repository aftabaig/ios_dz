//
//  DZViewController.m
//  DZ
//
//  Created by Ikarma Khan on 28/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZLeasesVC.h"
#import "DZSheetsVC.h"
#import "DZLeasesModel.h"
#import "ZAActivityBar.h"

@interface DZLeasesVC ()<UITableViewDataSource,UITableViewDelegate,DZLeasesManagerDelegate>
{
    ZAActivityBar *activityBar;
}

- (IBAction)btnLogoutTapped;

@property (strong,nonatomic)NSArray *leases;
@end


@implementation DZLeasesVC

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(initialze) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl beginRefreshing];

    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.4627 green:0.745 blue:0.3411 alpha:1.0];
    
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
    
}

- (void) viewDidAppear:(BOOL)animated{

    [super viewDidAppear:YES];
    
    self.tableView.userInteractionEnabled = NO;
    
    [self initialze];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];

}

#pragma mark- -LeaseManager
- (void) initialze{
    
    [ZAActivityBar showWithStatus:@"Loading Leases..."];
    
    DZLeasesManager *leaseManager = [[DZLeasesManager alloc]init];
    leaseManager.delegate = self;
    [leaseManager requestLeases];
    
}

#pragma mark- -LeaseManagerDelegate

- (void) leaseManager:(DZLeasesManager *)manager didFailToReceiveLeases:(NSError *)error{

    [self showErrorAlert:error];
    
}

- (void) leaseManager:(DZLeasesManager *)manager didReceiveLeases:(NSArray *)leases error:(NSError *)error{

    [self.refreshControl endRefreshing];
    
    if(error != nil)
        [self showErrorAlert:error];

    else{
        self.leases = leases;
        [self.tableView reloadData];
    }
}

- (void)showErrorAlert:(NSError*)error{

    [ZAActivityBar dismiss];
    self.tableView.userInteractionEnabled = YES;
    [self.refreshControl endRefreshing];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Operation Failed"
                                                   message:error.localizedDescription delegate:self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Try Again",
                          nil];
    [alert show];
}
#pragma mark- -TableViewDaraSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.leases count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}

    DZLeasesModel *leasesModel = [self.leases objectAtIndex:indexPath.row];
	NSString *cellName = leasesModel.name;

	cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = cellName;
    [[cell textLabel] setFont:[UIFont fontWithName:kAppFont size: 20.0]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    return view;
}

#pragma mark- -TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
    DZLeasesModel *leasesModel = [self.leases objectAtIndex:indexPath.row];
    NSInteger leaseID = leasesModel.leaseID;
    
    [self performSegueWithIdentifier:@"leasesToSheetsView" sender:[NSNumber numberWithInt:leaseID]];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        [ZAActivityBar dismiss];
        self.tableView.userInteractionEnabled = YES;
    }
}

#pragma mark- -UIControl

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    DZSheetsVC *sheetsController= [segue destinationViewController];
    [sheetsController setLeaseID:[sender integerValue]];
}

- (IBAction)btnLogoutTapped{
    
    // logout call
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark- -UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        [self initialze];
    }
    else if (buttonIndex ==0)
        [ZAActivityBar dismiss];
}
@end













