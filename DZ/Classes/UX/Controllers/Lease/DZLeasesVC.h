//
//  DZViewController.h
//  DZ
//
//  Created by Ikarma Khan on 28/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DZLeasesModel.h"
#import "DZLeasesManager.h"

@interface DZLeasesVC : UITableViewController<UIAlertViewDelegate>
{
    
}

@property (nonatomic,strong)NSString *apiKey;
@end
