//
//  DZLogInVC.m
//  DZ
//
//  Created by Ikarma Khan on 23/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZLogInVC.h"
#import "DZLeasesVC.h"

@interface DZLogInVC () <UITextFieldDelegate>
{
    
    IBOutlet UITextField *txtUsername;
    IBOutlet UITextField *txtPassword;
    IBOutlet UIButton *btnLogin;
    IBOutlet UISwitch *switchRememberMe;
    
    NSString *userName;
    NSString *password;
}
-(IBAction)btnLogInTapped:(id)sender;
-(IBAction)switchRememberMeChanged: (id)sender;
@end



@implementation DZLogInVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.4627 green:0.745 blue:0.3411 alpha:1.0];
    
    
}

- (void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear: animated];
    
    userName = [[NSString alloc]init];
    password = [[NSString alloc]init];
    
    [txtUsername becomeFirstResponder];
    switchRememberMe.on = [[NSUserDefaults standardUserDefaults]boolForKey:@"isRememberMe"];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchRememberMeChanged:(id)sender{
    
    [[NSUserDefaults standardUserDefaults]setBool:switchRememberMe.on forKey:@"isRememberMe"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (IBAction)btnLogInTapped:(id)sender{
    
    
    
    // NSLog(@"userName %@ : Password: %@",txtUsername.text,txtPassword.text);
    
    if(!([txtPassword.text isEqualToString:@"123"] && [txtUsername.text isEqualToString:@"test"])){
        
        [self loginFailedAlert];
        return;
    }
    
    [self performSegueWithIdentifier:@"LoginToLeasesView" sender:[NSNumber numberWithInt:0]];
    
}

- (void)loginFailedAlert{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login Failed" message:@"Username or password is incorrect. Please try again." delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil, nil];
    
    [alert show];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if(!switchRememberMe.on){
        txtUsername.text = @"";
        txtPassword.text = @"";
    }
   
    
   // DZLeasesVC *leasesController = [segue destinationViewController];
  //  [sheetsController setLeaseID:[sender integerValue]];
}

#pragma mark- -UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{

    if(textField == txtUsername){
        userName = txtUsername.text;
        [txtPassword becomeFirstResponder];
    }
    else if(textField == txtPassword){
        password = txtPassword.text;
        [textField resignFirstResponder];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}



@end
