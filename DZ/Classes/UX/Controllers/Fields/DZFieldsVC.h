//
//  DZFieldsViewController.h
//  DZ
//
//  Created by Ikarma Khan on 30/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDSpreadView.h"

@interface DZFieldsVC : UIViewController<UIAlertViewDelegate>

@property (nonatomic) BOOL isSubSheetField;
@property (nonatomic) NSInteger sheetID;
@property (nonatomic) NSInteger subSheetID;
@property (nonatomic) NSInteger leaseID;
@property (nonatomic,strong)IBOutlet MDSpreadView *spreadSheet;
@end
