//
//  DZFieldsViewController.m
//  DZ
//
//  Created by Ikarma Khan on 30/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZFieldsVC.h"

#import "DZFieldsModel.h"
#import "DZFieldsManager.h"

#import "DZUpdateFieldVC.h"



#import "ZAActivityBar.h"
#import "DZNetworkEngine.h"

@interface DZFieldsVC ()<DZFieldManagerDelegate,MDSpreadViewDataSource,MDSpreadViewDelegate>{
    
    
    IBOutlet UILabel *lblCurrentMonth;
    IBOutlet UIView *containerView;
    NSMutableArray *columnHeads;
    NSMutableString *strType;
    
    int monthOffset;
    NSArray *arrayMonth;
    NSDate *newDate;
    NSTimer *timer;
    BOOL flagSpreadViewStoppedUpdating;
}

- (IBAction)btnNextMonthTapped:(id)sender;
- (IBAction)btnPreviousMonthTapped:(id)sender;

@property (nonatomic,strong) NSArray *fields;

@end


@implementation DZFieldsVC

@synthesize spreadSheet = _spreadSheet;

#pragma mark- -MDSpreadView

- (NSInteger)spreadView:(MDSpreadView *)aSpreadView numberOfColumnsInSection:(NSInteger)section
{
    return columnHeads.count;
}

- (NSInteger)spreadView:(MDSpreadView *)aSpreadView numberOfRowsInSection:(NSInteger)section
{
    return self.fields.count;
}

- (CGFloat)spreadView:(MDSpreadView *)aSpreadView heightForRowAtIndexPath:(MDIndexPath *)indexPath
{
    
    return 60.0;
}

- (CGFloat)spreadView:(MDSpreadView *)aSpreadView heightForRowHeaderInSection:(NSInteger)rowSection
{
    return 50.0;
}
- (CGFloat)spreadView:(MDSpreadView *)aSpreadView widthForColumnAtIndexPath:(MDIndexPath *)indexPath
{
    
    if (columnHeads.count == 1) {
        return 240.0;
    }
    return 150.0;
}

- (CGFloat)spreadView:(MDSpreadView *)aSpreadView heightForRowFooterInSection:(NSInteger)rowSection
{
    return 0;
}


- (CGFloat)spreadView:(MDSpreadView *)aSpreadView widthForColumnHeaderInSection:(NSInteger)columnSection
{
    return 60.0;
}

- (CGFloat)spreadView:(MDSpreadView *)aSpreadView widthForColumnFooterInSection:(NSInteger)columnSection
{
    return 0;
}

#pragma mark- -MDSpreadSheetCells

- (MDSpreadViewCell *)spreadView:(MDSpreadView *)aSpreadView cellForRowAtIndexPath:(MDIndexPath *)rowPath forColumnAtIndexPath:(MDIndexPath *)columnPath
{
    
    static NSString *cellIdentifier = @"Cell";
    MDSpreadViewCell *cell = [aSpreadView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MDSpreadViewCell alloc] initWithStyle:MDSpreadViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:kAppFont size:20.0];
    }
    
    NSDictionary *fieldInfo = [[self getFieldInfoForRowAtIndex:rowPath.row] objectAtIndex:columnPath.column];
    
    strType= [[[NSString alloc]initWithString:[fieldInfo objectForKey:@"data_type"]]mutableCopy];
    
    if([strType isEqualToString:@"Numeric"]){
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if (rowPath.row >= 25) {
            cell.textLabel.text = [self spreadView:aSpreadView objectValueForRowAtIndexPath:rowPath forColumnAtIndexPath:columnPath];
        }
        else{
            if([[fieldInfo objectForKey:@"value"] isEqualToString:@"#*#*"])
                cell.textLabel.text = @"0";
            else
                cell.textLabel.text = [fieldInfo objectForKey:@"value"];
        }
    }else if([strType isEqualToString:@"Text"]){
        if (rowPath.row >= 25) {
            
            cell.textLabel.text = [self spreadView:aSpreadView objectValueForRowAtIndexPath:rowPath forColumnAtIndexPath:columnPath];
        }
        else{
            if([[fieldInfo objectForKey:@"value"] isEqualToString:@"#*#*"])
                cell.textLabel.text = @"";
            else
                cell.textLabel.text = [fieldInfo objectForKey:@"value"];
        }
    }
    
    BOOL isEnabled =([self dayOfMonth:0] == rowPath.row+1 && monthOffset==0) ? YES :NO;
    cell.userInteractionEnabled = isEnabled;
    cell.textLabel.enabled = isEnabled;
    flagSpreadViewStoppedUpdating = FALSE;
    return cell;
    
}


- (id)spreadView:(MDSpreadView *)aSpreadView objectValueForRowAtIndexPath:(MDIndexPath *)rowPath forColumnAtIndexPath:(MDIndexPath *)columnPath
{
    NSDictionary *fieldInfo = [[self getFieldInfoForRowAtIndex:rowPath.row] objectAtIndex:columnPath.column];
 
    
    if([[fieldInfo objectForKey:@"value"] isEqualToString:@"#*#*"]){

        if([strType isEqualToString:@"Numeric"])
            return @"0";
        else
            return @"";
    }
    else
        return [fieldInfo objectForKey:@"value"];
}

-(NSArray*)getFieldInfoForRowAtIndex:(NSInteger)rowNum{
    
    DZFieldsModel *fieldModel = [self.fields objectAtIndex:rowNum];
    return fieldModel.fieldsInfo;
}

- (CGFloat)customRowHeight:(MDIndexPath*)row column:(MDIndexPath*)colum{
    
    NSDictionary *fieldInfo = [[self getFieldInfoForRowAtIndex:row.row] objectAtIndex:colum.column];
    
    //Get a reference to your string to base the cell size on.
    NSString *cellText = [fieldInfo objectForKey:@"value"];
    
    //set the desired size of your textbox
    CGSize constraint = CGSizeMake(320.0, MAXFLOAT);
    
    //set your text attribute dictionary
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:22.0] forKey:NSFontAttributeName];
    
    //get the size of the text box
    CGRect textsize = [cellText boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    //calculate your size
    float textHeight = textsize.size.height +20;
    
    //I have mine set for a minimum size
    textHeight = (textHeight < 40.0) ? 40 : textHeight;
    return textHeight;
    
}

- (id)spreadView:(MDSpreadView *)aSpreadView titleForHeaderInRowSection:(NSInteger)rowSection forColumnSection:(NSInteger)columnSection
{
    return @"Day";
}
- (id)spreadView:(MDSpreadView *)aSpreadView titleForHeaderInColumnSection:(NSInteger)section forRowAtIndexPath:(MDIndexPath *)rowPath
{
    DZFieldsModel *fieldModel = [self.fields objectAtIndex:rowPath.row];
    return [NSString stringWithFormat:@"%ld",(long)fieldModel.day];
}
- (id)spreadView:(MDSpreadView *)aSpreadView titleForHeaderInRowSection:(NSInteger)section forColumnAtIndexPath:(MDIndexPath *)columnPath
{
    return [columnHeads objectAtIndex:columnPath.column];
}
- (void)spreadView:(MDSpreadView *)aSpreadView didSelectCellForRowAtIndexPath:(MDIndexPath *)rowPath forColumnAtIndexPath:(MDIndexPath *)columnPath
{
    [aSpreadView deselectCellForRowAtIndexPath:rowPath forColumnAtIndexPath:columnPath animated:YES];
    NSString *strEditPath = [NSString stringWithFormat:@"%ld,%d",(long)rowPath.row,columnPath.column];
    [self performSegueWithIdentifier:@"fieldsToUpdateFieldView" sender:strEditPath];
}
/*
 - (void)spreadView:(MDSpreadView *)aSpreadView willDisplayCell:(MDSpreadViewCell *)cell forRowAtIndexPath:(MDIndexPath *)rowPath forColumnAtIndexPath:(MDIndexPath *)columnPath{
 
 //NSLog(@"check: %d : %d %d",rowPath.row,[[[aSpreadView indexPathsForVisibleRows]lastObject]integerValue],columnPath.column);
 //
  if(rowPath.row == [[[aSpreadView indexPathsForVisibleRows]lastObject]integerValue]){
 
    if(columnPath.column == [[[aSpreadView indexPathsForVisibleColumns]lastObject]integerValue]){
 
  }
  }
 }
 */
- (void)spreadViewStoppedUpdating{
    if(flagSpreadViewStoppedUpdating)
    {
        [timer invalidate];
        timer = nil;
        
        [ZAActivityBar dismiss];
        [self.navigationItem setHidesBackButton:NO animated:NO];
        self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
        self.view.userInteractionEnabled = TRUE;
        [self.spreadSheet visibleRowsShouldClear:TRUE];
    }
}

- (MDSpreadViewSelection *)spreadView:(MDSpreadView *)aSpreadView willHighlightCellWithSelection:(MDSpreadViewSelection *)selection
{
    return [MDSpreadViewSelection selectionWithRow:selection.rowPath column:selection.columnPath mode:MDSpreadViewSelectionModeRowAndColumn];
}

- (MDSpreadViewSelection *)spreadView:(MDSpreadView *)aSpreadView willSelectCellWithSelection:(MDSpreadViewSelection *)selection
{
    return [MDSpreadViewSelection selectionWithRow:selection.rowPath column:selection.columnPath mode:MDSpreadViewSelectionModeRowAndColumn];
}



#pragma mark- -UIControls

- (IBAction)btnNextMonthTapped:(id)sender{
    
    monthOffset++;
    [self initialize];
}
- (IBAction)btnPreviousMonthTapped:(id)sender{
    
    monthOffset--;
    [self initialize];
}

- (void) setCurrentMonthLabel{
    
    NSString *strDate = [self getMonth:monthOffset];
    NSArray *dateComponents = [[NSArray alloc]initWithArray:[strDate componentsSeparatedByString:@" "]];
    NSInteger month = [[dateComponents objectAtIndex:0]integerValue];
    NSInteger year = [[dateComponents objectAtIndex:2]integerValue];
    
    lblCurrentMonth.text = [NSString stringWithFormat:@"%@ %d",[arrayMonth objectAtIndex:month-1],year];
    
    
}

#pragma mark- -viewLifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationLandscapeLeft ||
        [[UIDevice currentDevice]orientation] == UIInterfaceOrientationLandscapeRight)
    {
        CGRect frame = containerView.frame;
        frame.origin.y = 50;
        containerView.frame = frame;
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    arrayMonth = [[NSArray alloc]
                  initWithObjects:@"January",
                  @"Feburay",
                  @"March",
                  @"April",
                  @"May",
                  @"June",
                  @"July",
                  @"August",
                  @"September",
                  @"October",
                  @"November",
                  @"December",
                  nil];
    
    monthOffset  = 0;
    
    [self initialize];
}

- (void)initialize{
    
    [ZAActivityBar showWithStatus:@"Loading Fields..."];
   
    flagSpreadViewStoppedUpdating = FALSE;
    self.view.userInteractionEnabled = FALSE;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(spreadViewStoppedUpdating) userInfo:nil repeats:YES];
    
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
    
    [self setCurrentMonthLabel];
    
    DZFieldsManager *fieldsManager = [[DZFieldsManager alloc]init];
    fieldsManager.leaseID = self.leaseID;
    fieldsManager.isSubSheetField = self.isSubSheetField;
    
    NSString *monthYear = [self getMonth:monthOffset];
    
    fieldsManager.month = [[[monthYear componentsSeparatedByString:@" "]objectAtIndex:0]integerValue];
    fieldsManager.year = [[[monthYear componentsSeparatedByString:@" "]objectAtIndex:2]integerValue];
    
    if(self.isSubSheetField)
        fieldsManager.subSheetID = self.subSheetID;
    else
        fieldsManager.sheetID = self.sheetID;
    
    fieldsManager.delegate = self;
    [fieldsManager requestFields];
    
}
- (void) viewWillDisappear:(BOOL)animated{
    
    
    [super viewWillDisappear:YES];
    
    
    
}
- (void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    if(timer != nil){
        [timer invalidate];
        //  timer = nil;
    }
    // self.spreadSheet = nil;
    //self.fields = nil;
    
    
    // [NSObject cancelPreviousPerformRequestsWithTarget:self.spreadSheet selector:@selector(reloadData) object:nil];
    
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSString *strEditPath = (NSString*)sender;
    NSArray *editParameters = [strEditPath componentsSeparatedByString:@","];
    
    DZUpdateFieldVC *updateFieldView = [segue  destinationViewController];
    updateFieldView.fields = [self getFieldInfoForRowAtIndex:[[editParameters objectAtIndex:0]integerValue]];
    updateFieldView.strType = strType;
    updateFieldView.editParameters = editParameters;
    updateFieldView.sheetID = self.sheetID;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:[NSNumber numberWithInteger:[[editParameters objectAtIndex:1]integerValue]] forKey:@"fieldName"];
    [prefs synchronize];
    
}

#pragma mark- -FieldsManagerDelegate

- (void) fieldManager:(DZFieldsManager *)manager didFailReceiveFields:(NSError *)error{
    
    
    NSLog(@"Error - Fields: %@",error.localizedDescription);
    flagSpreadViewStoppedUpdating = FALSE;
    [self showErrorAlert:error];
}

- (void) fieldManager:(DZFieldsManager *)manager didReceiveFields:(NSArray *)fields error:(NSError*)error{
    
    if(error!=nil){
        [self.navigationItem setHidesBackButton:NO animated:NO];
        [self showErrorAlert:error];
    }
    else
    {
        
        self.fields = fields;
        [self fetchColumnHeads];
        self.spreadSheet.delegate = self;
        self.spreadSheet.dataSource = self;
        // self.spreadSheet.scrollIndicatorInsets = self.spreadSheet.contentInset;
        self.spreadSheet.showsHorizontalScrollIndicator = FALSE;
        [self.spreadSheet reloadData];
        
    }
    flagSpreadViewStoppedUpdating = TRUE;
}


- (void)showErrorAlert:(NSError*)error{
    
    [ZAActivityBar dismiss];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Operation Failed" message:error.localizedDescription delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again", nil];
    
    [alert show];
    
    
}
#pragma mark- -helperFunction
- (NSMutableArray*)fetchColumnHeads{
    
    columnHeads = [[NSMutableArray alloc]init];
    
    DZFieldsModel *fieldModel = [self.fields objectAtIndex:0];
    NSArray *fieldsInfoAll = fieldModel.fieldsInfo;
    
    for (NSDictionary *dictFields in fieldsInfoAll){
        [columnHeads addObject:[dictFields objectForKey:@"name"]];
    }
    return columnHeads;
}

- (NSString*)getMonth :(int) offset{
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendarUnit unitFlags = NSMonthCalendarUnit | NSYearCalendarUnit;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:currentDate];
    
    NSInteger month = [dateComponents month];
    NSInteger year = [dateComponents year];
    
    [dateComponents setMonth:offset];
    [dateComponents setYear:0];
    newDate = [calendar dateByAddingComponents:dateComponents toDate:currentDate options:0];
    
    NSDateComponents * newDateComponents = [calendar components:unitFlags fromDate:newDate];
    
    month = [newDateComponents month];
    year = [newDateComponents year];
    
    return [NSString stringWithFormat:@"%d  %d",month,year];
}

- (NSInteger)dayOfMonth:(NSInteger)rowNum{
    
    NSDate *currentDate = [NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSCalendarUnit unitFlags = NSDayCalendarUnit;
    
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:currentDate];
    
    NSInteger day = [dateComponents day];
    
    [dateComponents setDay:rowNum];
    
    newDate = [calendar dateByAddingComponents:dateComponents toDate:currentDate options:0];
    
    NSDateComponents * newDateComponents = [calendar components:unitFlags fromDate:newDate];
    
    day = [newDateComponents day];
    
    return day;
}

#pragma mark- -UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        
        self.fields = nil;
        self.fields = [[NSMutableArray alloc]init];
        [self initialize];
    }
    
    else if (buttonIndex ==0)
        [ZAActivityBar dismiss];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        CGRect frame = containerView.frame;
        frame.origin.y = 50;
        
        containerView.frame = frame;
    }
    else
    {
        CGRect frame = containerView.frame;
        frame.origin.y = 62;
        containerView.frame = frame;
    }
}


/*
 - (id)spreadView:(MDSpreadView *)aSpreadView titleForHeaderInRowSection:(NSInteger)rowSection forColumnFooterSection:(NSInteger)columnSection
 {
 return [NSString stringWithFormat:@"B Cor %ld-%ld", (long)(columnSection+1), (long)(rowSection+1)];
 }
 
 - (id)spreadView:(MDSpreadView *)aSpreadView titleForHeaderInColumnSection:(NSInteger)columnSection forRowFooterSection:(NSInteger)rowSection
 {
 return [NSString stringWithFormat:@"A Cor %ld-%ld", (long)(columnSection+1), (long)(rowSection+1)];
 }
 
 - (id)spreadView:(MDSpreadView *)aSpreadView titleForFooterInRowSection:(NSInteger)rowSection forColumnSection:(NSInteger)columnSection
 {
 return [NSString stringWithFormat:@"F Cor %ld-%ld", (long)(columnSection+1), (long)(rowSection+1)];
 }
 
 - (id)spreadView:(MDSpreadView *)aSpreadView titleForFooterInRowSection:(NSInteger)section forColumnAtIndexPath:(MDIndexPath *)columnPath
 {
 return [NSString stringWithFormat:@"F Row %ld (%ld-%ld)", (long)(section+1), (long)(columnPath.section+1), (long)(columnPath.row+1)];
 }
 
 - (id)spreadView:(MDSpreadView *)aSpreadView titleForFooterInColumnSection:(NSInteger)section forRowAtIndexPath:(MDIndexPath *)rowPath
 {
 return [NSString stringWithFormat:@"F Col %ld (%ld-%ld)", (long)(section+1), (long)(rowPath.section+1), (long)(rowPath.row+1)];
 }
 
 
 NSDate *date = [NSDate date];
 
 NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
 
 NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit| NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSTimeZoneCalendarUnit;
 
 float xPos = 20.0;
 float yPos = 75.0;
 float xPos2 = 35.0;
 float screenHeight =   [[UIScreen mainScreen]bounds].size.height;
 
 NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:date];
 NSArray *arrayMonth = [[NSArray alloc]
 initWithObjects:@"January",@"Feburay",@"March"
 ,@"April",@"May",@"June",@"July"
 ,@"August",
 @"September",
 @"October",
 @"November",@"December",nil];
 int montNum = [dateComponents month];
 int day = [dateComponents day];
 
 
 NSLog(@"FieldsInfo:%@",fieldsInfo);
 
 yPos += 60.0;
 
 UIScrollView *scroller = [[UIScrollView alloc]init];
 scroller.frame = CGRectMake(0.0, 30.0, 320.0,screenHeight);
 for(int i=1;i< 10;i++){
 xPos2 = 35.0;
 UILabel *lblDate = [[UILabel alloc]init];
 
 lblDate.frame = CGRectMake(xPos, yPos - 10.0, 30.0, 40.0);
 lblDate.text = [NSString stringWithFormat:@"%d: ",day];
 
 [scroller addSubview:lblDate];
 
 for(NSDictionary *dict in fieldsInfo){
 
 UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(xPos2, yPos, 40.0, 60.0)];
 lblName.text = [NSString stringWithFormat:@"%@:",[dict objectForKey:@"name"]];
 [lblName sizeToFit];
 [scroller addSubview:lblName];
 
 if([[dict objectForKey:@"data_type"] isEqualToString:@"Numeric"])
 {
 UISlider *slider = [[UISlider alloc]init ];
 slider.frame =CGRectMake(xPos2, yPos+ lblName.bounds.size.height,60.0,slider.bounds.size.height);
 slider.minimumValue = [[dict objectForKey:@"main_value"]floatValue];
 slider.maximumValue = [[dict objectForKey:@"max_value"]floatValue];
 
 [scroller addSubview:slider];
 xPos2 += 45.0;
 }
 else if([[dict objectForKey:@"data_type"] isEqualToString:@"Text"]){
 
 UITextField *txtField = [[UITextField alloc]init];
 txtField.frame = CGRectMake(xPos2, yPos+lblName.bounds.size.height,100.0,50.0);
 txtField.placeholder = @"Enter text..";
 txtField.delegate=self;
 [scroller addSubview:txtField];
 xPos2 +=100.0;
 }
 
 xPos2 += lblName.bounds.size.width;
 }
 yPos += 60.0;
 
 NSDate *dateN;
 
 [dateComponents setDay:i];
 
 dateN = [calendar dateByAddingComponents:dateComponents toDate:date options:0];
 dateComponents = [calendar components:unitFlags fromDate:dateN];
 day = [dateComponents day];
 }
 scroller.directionalLockEnabled = YES;
 scroller.contentSize = CGSizeMake(xPos2, yPos);
 [self.view addSubview:scroller];
 
 NSString *monthName = [arrayMonth objectAtIndex:montNum-1];
 
 UILabel *lblMonthName = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 65.0, 320.0, 60.0)];
 lblMonthName.text  = monthName;
 lblMonthName.textAlignment = NSTextAlignmentCenter;
 lblMonthName.backgroundColor = [UIColor whiteColor];
 
 [self.view addSubview:lblMonthName];
 */

@end
