//
//  DZUpdateFieldCell.m
//  DZ
//
//  Created by Ikarma Khan on 20/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZUpdateFieldCell.h"
#import "CustomUITextField.h"
#import <QuartzCore/QuartzCore.h>

@interface DZUpdateFieldCell()<UITextFieldDelegate>{
    
  
    
    
}


@end

@implementation DZUpdateFieldCell

@synthesize lblSliderValue,lblNameOfField,sliderValue,txtNotes,txtValue;


- (id)initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    
    if (self) {
        lblNameOfField  = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 5.0, 250.0, 35.0)];
        lblNameOfField.font = [UIFont fontWithName:kAppFont size:20.0];
        [self addSubview:lblNameOfField];
        lblNameOfField.text = [dict objectForKey:@"name"];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        
        if([[dict objectForKey:@"data_type"]isEqualToString:@"Numeric"])
        {
            txtValue = [[CustomUITextField alloc]initWithFrame:CGRectMake(10.0, 40.0, 240.0, 35.0)];
            txtValue.leftView = paddingView;
            txtValue.leftViewMode = UITextFieldViewModeAlways;
            txtValue.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
            txtValue.text = [dict objectForKey:@"value"];
            txtValue.placeholder = [dict objectForKey:@"value"];
            txtValue.delegate = self;
            txtValue.keyboardType = UIKeyboardTypeDecimalPad;
            txtValue.maxValue = [[dict objectForKey:@"max_value"]floatValue];
            txtValue.minValue = [[dict objectForKey:@"min_value"]floatValue];
            
            txtValue.layer.borderColor = [[UIColor grayColor]CGColor];
            txtValue.layer.borderWidth = 0.5;
            txtValue.layer.cornerRadius = 5.0;
            txtValue.font = [UIFont fontWithName:kAppFont size:15.0];
            
            txtValue.clearButtonMode = UITextFieldViewModeWhileEditing;
            
            lblSliderValue = [[UILabel alloc]initWithFrame:CGRectMake(260.0, 38.0, 60.0, 35.0)];
            lblSliderValue.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            lblSliderValue.textAlignment = NSTextAlignmentLeft;
            lblSliderValue.font = [UIFont fontWithName:kAppFont size:20.0];
            lblSliderValue.text = [NSString stringWithFormat:@"%.0f",txtValue.maxValue];
            
            [self addSubview:txtValue];
            [self addSubview:lblSliderValue];
        }
        else if ([[dict objectForKey:@"data_type"]isEqualToString:@"Text"])
        {
            
            txtNotes = [[UITextField alloc]initWithFrame:CGRectMake(10.0, 20.0, 245.0, 75.0)];
            txtNotes.delegate = self;
            txtNotes.returnKeyType = UIReturnKeyDone;
            if([[dict objectForKey:@"value"]isEqualToString:@"#*#*"])
                txtNotes.text = @"";
            else
                txtNotes.text = [dict objectForKey:@"value"];
            
            [txtNotes becomeFirstResponder];
            
            [self addSubview:txtNotes];
        }
        
    }
    self.cellID = [[dict objectForKey:@"rowID"]integerValue];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

- (IBAction)sliderValueChanged:(id)sender{
    
    lblSliderValue.text = [NSString stringWithFormat:@"%.0f",sliderValue.value];

    if(self.delegate)
        [self.delegate updateFieldValue:self updateValue:[NSString stringWithFormat:@"%.0f",sliderValue.value]];


}

#pragma mark- -UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if([textField.text isEqualToString:@""] && textField == txtValue)
        textField.text = 0;
    
    if(self.delegate)
        [self.delegate updateFieldValue:self updateValue:textField.text];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    
    if([textField isKindOfClass:[CustomUITextField class]]){
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL flag =  [string isEqualToString:filtered];
        
        if(!flag)
            return FALSE;
        
        
        CustomUITextField *txtField =(CustomUITextField*) textField;
        if([string isEqualToString:@"."] &&
           [textField.text floatValue] < txtField.maxValue &&
           [textField.text floatValue] > txtField.minValue)
            return TRUE;
        
        float txtFieldValue = 0.0;
        
        if(![string isEqualToString:@""] ){
            if(([textField.text rangeOfString:@"."].location == NSNotFound))
            {
                if(txtField.text.length == 0)
                    txtFieldValue = [string floatValue];
                else if(txtField.text.length == 1)
                    txtFieldValue = powf(10, 1) * [txtField.text floatValue] +[string floatValue];
                else
                    txtFieldValue =  powf(10, txtField.text.length - 1) * [txtField.text floatValue] +[string floatValue];
               
                if(txtFieldValue > txtField.maxValue || txtFieldValue < txtField.minValue){
                    
                    return FALSE;
                }
            }
        }
    }
    
    if(self.delegate)
        [self.delegate updateFieldValue:self updateValue:textField.text];

    return YES;
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


@end
