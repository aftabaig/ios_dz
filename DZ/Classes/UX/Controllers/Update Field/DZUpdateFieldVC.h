//
//  DZUpdateFieldVC.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DZUpdateFieldVC : UITableViewController



@property (nonatomic,strong) NSArray *fields;
@property (nonatomic,strong)NSString *strType;
@property (nonatomic) NSArray* editParameters;
@property (nonatomic)NSInteger sheetID;
@end
