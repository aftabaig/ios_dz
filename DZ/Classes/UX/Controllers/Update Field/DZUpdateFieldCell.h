//
//  DZUpdateFieldCell.h
//  DZ
//
//  Created by Ikarma Khan on 20/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUITextField.h"
@class DZUpdateFieldCell;

@protocol DZUpdateCellDelegate

@required
- (void)updateFieldValue:(DZUpdateFieldCell*)updatedCell updateValue:(NSString*)updatedValue;
@end

@interface DZUpdateFieldCell : UITableViewCell

@property (nonatomic,strong) IBOutlet CustomUITextField *txtValue;

@property (nonatomic,strong) IBOutlet UILabel *lblNameOfField;
@property (nonatomic,strong) IBOutlet UISlider *sliderValue;

@property (nonatomic,strong)IBOutlet UILabel *lblSliderValue;
@property (nonatomic,strong) IBOutlet UITextField *txtNotes;

@property (weak,nonatomic) id<DZUpdateCellDelegate>delegate;

@property (nonatomic) NSInteger cellID;


- (IBAction)sliderValueChanged:(id)sender;
- (id)initWithDictionary:(NSDictionary*)dict;

@end

