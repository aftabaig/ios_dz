//
//  DZUpdateFieldVC.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZUpdateFieldVC.h"

#import "DZFieldsModel.h"
#import "DZFieldsVC.h"
#import "DZUpdateFieldManager.h"
#import "DZUpdateFieldCell.h"

@interface DZUpdateFieldVC ()<UITableViewDataSource,UITableViewDelegate,DZUpdateFieldManagerDelegate,UIAlertViewDelegate,UITextFieldDelegate,DZUpdateCellDelegate>
{
    NSMutableArray *updatedFields;
    IBOutlet UIBarButtonItem* btnUpdate;
}
- (IBAction)btnUpdateTapped:(id)sender;

@end

@implementation DZUpdateFieldVC

#pragma mark- -ViewLifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.4627 green:0.745 blue:0.3411 alpha:1.0];
    
    updatedFields = [[NSMutableArray alloc]init];
    [self createDummyArray];
}
- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
}
- (void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    [updatedFields removeAllObjects];
}

#pragma mark- -helperFunction
- (NSString*)dateString{
    
    NSDate *localDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    return [dateFormatter stringFromDate: localDate];
    
}

- (NSString*)timeString{
    
    NSDate *localDate = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    timeFormatter.dateFormat = @"HH:mm:ss";
    
    return [timeFormatter stringFromDate: localDate];
    
}

#pragma mark- -UIControls

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    

}

- (IBAction)btnUpdateTapped:(id)sender{

    for(int i=0; i<self.fields.count;i++){
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        DZUpdateFieldCell *cell = (DZUpdateFieldCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        if(cell.txtNotes){
            if([cell.txtNotes isFirstResponder])
               [cell.txtNotes resignFirstResponder];
        }
    }
    
    [self updateFields];
}

- (void) popUpdateFieldView{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- -UpdateFieldViewDelegate

- (void)updateFieldManager:(DZUpdateFieldManager *)didFailToUpdateFieldManager :(NSError *)error{
    
    NSLog(@"error: %@",error.localizedDescription);
    [self showErrorAlert:error];
}

- (void)updateFieldManager:(DZUpdateFieldManager *)updateFieldManager didUpdateFields:(BOOL)status error:(NSError *)error{
    
    if(status){
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Fields updated successfully." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        alert.tag = 0;
    }
}
- (void)showErrorAlert:(NSError*)error{
    
    [updatedFields removeAllObjects];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Update Failed" message:error.localizedDescription delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again", nil];
    
    [alert show];
    alert.tag = 1;
    
    
}
#pragma mark- -prepareDataToBeUpdated

- (void)addUpdatedFields{
    /*
    
    NSDictionary *dictField = [self.fields objectAtIndex:index];
    NSMutableString *strValue = [[NSMutableString alloc]init];
    
    if([[dictField objectForKey:@"data_type"]isEqualToString:@"Numeric"]){
        strValue = [[NSString stringWithFormat:@"%.0f",cell.sliderValue.value]mutableCopy];
    }
    else if ([[dictField objectForKey:@"data_type"]isEqualToString:@"Text"]){
        strValue = [cell.txtNotes.text mutableCopy];
    }
    
    int fieldID =[[dictField objectForKey:@"field_id"] integerValue];
    BOOL alreadyHasValue = ([[dictField objectForKey:@"value"] isEqualToString:@""])? FALSE : TRUE;
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setObject:[self timeString] forKey:@"time"];
    [dict setObject:[self dateString] forKey:@"date"];
    [dict setObject:[NSString stringWithFormat:@"%d",self.sheetID] forKey:@"sheet"];
    [dict setObject:[NSString stringWithFormat:@"%d",fieldID] forKey:@"field"];
    [dict setObject:strValue forKey:@"value"];
    
    if(alreadyHasValue)
        [dict setObject:[dictField objectForKey:@"value_id"] forKey:@"id"];
    
    
    [updatedFields addObject:dict];
    
    NSLog(@"updatedFields: %@",updatedFields);
     */
}

- (void)createDummyArray{
 
    for(NSDictionary* tD in self.fields){
        
        NSMutableDictionary *tDN = [[NSMutableDictionary alloc]init];
        NSString *value;
        if([[tD objectForKey:@"data_type"]isEqualToString:@"Numeric"])
            value = ([[tD objectForKey:@"value"]isEqualToString:@""]) ? @"0" : [tD objectForKey:@"value"];
        else
            value = ([[tD objectForKey:@"value"]isEqualToString:@""]) ? @"#*#*" : [tD objectForKey:@"value"];
        
        [tDN setObject:[tD objectForKey:@"field_id"] forKey:@"field"];
        [tDN setObject:[NSNumber numberWithInt:self.sheetID] forKey:@"sheet"];
        [tDN setObject:value forKey:@"value"];
        [tDN setObject:[self timeString] forKey:@"time" ];
        [tDN setObject:[self dateString] forKey:@"date"];

        if(!([[tD objectForKey:@"value"] isEqualToString:@""]))
           [tDN setObject:[tD objectForKey:@"value_id"] forKey:@"id"];
        
        [updatedFields addObject:tDN];
    }
    
}

#pragma mark- -UpdateFields
- (void) updateFields{
    NSLog(@"%@",updatedFields);

    DZUpdateFieldManager *updateManager = [[DZUpdateFieldManager alloc]init];
    updateManager.delegate = self;
    updateManager.updatedFields = updatedFields;
    [updateManager postFields];
    
}

#pragma mark- -UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 0 && buttonIndex == 0){
        
        [self popUpdateFieldView];
    }
    else if(alertView.tag == 1 && buttonIndex == 1){
        
        [self updateFields];
    }
    else if(alertView.tag == 0 && buttonIndex == 1){
        
     //   [self.navigationItem setHidesBackButton:NO animated:NO];
     //   self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
    }
}

#pragma mark - UITableViewDatasource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fields.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSMutableDictionary *dict = [[[NSMutableDictionary alloc]initWithDictionary:[self.fields objectAtIndex:indexPath.row]]mutableCopy];
    [dict setObject:[NSNumber numberWithInt:indexPath.row] forKey:@"rowID"];
    [dict setObject:[[updatedFields objectAtIndex:indexPath.row]objectForKey:@"value"] forKey:@"value"];
    DZUpdateFieldCell *cell = [[DZUpdateFieldCell alloc]initWithDictionary:dict];
    cell.delegate = self;
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    return view;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark- -UpdateCellDelegate
- (void)updateFieldValue:(DZUpdateFieldCell *)updatedCell updateValue:(NSString *)updatedValue{
    
    NSMutableDictionary* dict = [[[NSMutableDictionary alloc]initWithDictionary:[updatedFields objectAtIndex:updatedCell.cellID]]mutableCopy];
  
    if([updatedValue isEqualToString:@""])
        updatedValue = @"#*#*";

    [dict setObject:updatedValue forKey:@"value"];
  
    NSDictionary *dictField = [self.fields objectAtIndex:updatedCell.cellID];

    BOOL alreadyHasValue = ([[dictField objectForKey:@"value"] isEqualToString:@""])? FALSE : TRUE;
    if(alreadyHasValue)
        [dict setObject:[dictField objectForKey:@"value_id"] forKey:@"id"];
    
    [updatedFields replaceObjectAtIndex:updatedCell.cellID withObject:dict];
}

/*
 #pragma mark- -TableViewDaraSource
 
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
 return 44.0;
 }
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
 
 if (tableView.tag == 1)
 return [self.fields count];
 else
 return 1;
 }
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
 static NSString *CellIdentifier = @"Cell";
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (cell == nil) {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 
 if(tableView.tag == 1)
 {
 NSDictionary *dictField =[self.fields objectAtIndex:indexPath.row];
 cell.textLabel.text = [dictField objectForKey:@"name"];
 //  txtNotes.text = [dictField objectForKey:@"value"];
 }
 else{
 
 int index = [prefs integerForKey:@"fieldName"];
 
 NSDictionary *dictField =[self.fields objectAtIndex:index];
 
 cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
 cell.textLabel.text = [dictField objectForKey:@"name"];
 
 //      sliderValue.maximumValue = [[dictField objectForKey:@"max_value"]floatValue];
 //      sliderValue.minimumValue = [[dictField objectForKey:@"min_value"] floatValue];
 //      sliderValue.value = [[dictField objectForKey:@"value"] floatValue];
 //      lblSliderValue.text = [NSString stringWithFormat:@"%.0f",sliderValue.value];
 }
 
 return cell;
 }
 
 
 #pragma mark- -UITableViewDaraSource
 
 - (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
 if(tableView.tag == 1){
 
 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
 [prefs setObject:[NSNumber numberWithInteger:indexPath.row] forKey:@"fieldName"];
 [prefs synchronize];
 
 [tableView deselectRowAtIndexPath:indexPath animated:YES];
 [tblNames removeFromSuperview];
 [tblName reloadData];
 return;
 }
 else
 [self displayFieldSelectionTable];
 
 [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
 
 }
 
 - (void) displayFieldSelectionTable{
 
 float tblHeight = 44.0*self.fields.count;
 
 if(self.fields.count > 10.0)
 tblHeight = tblHeight/2.0;
 
 tblNames = [[UITableView alloc]initWithFrame:CGRectMake(80.0, 70.0, 220.0, tblHeight)];
 tblNames.tag = 1;
 CGSize size = CGSizeMake(220.0, (44.0*self.fields.count));
 [tblNames setContentSize:size];
 tblNames.userInteractionEnabled = YES;
 tblNames.pagingEnabled = YES;
 tblNames.dataSource = self;
 tblNames.delegate = self;
 [self.view addSubview:tblNames];
 
 }
 */


@end




























