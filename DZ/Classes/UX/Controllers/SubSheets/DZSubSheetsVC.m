//
//  DZSubSheetViewController.m
//  DZ
//
//  Created by Ikarma Khan on 30/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZSubSheetsVC.h"

#import "DZSubSheetsModel.h"
#import "DZSubSheetsManager.h"

#import "DZFieldsVC.h"

#import "ZAActivityBar.h"

@interface DZSubSheetsVC ()<UITableViewDelegate,UITableViewDataSource,DZSubSheetsDelegate>

@property (nonatomic,strong) NSArray *subSheets;

@end

@implementation DZSubSheetsVC

#pragma mark- -viewLifeCycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(initialze) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl beginRefreshing];

    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.4627 green:0.745 blue:0.3411 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    [ZAActivityBar showWithStatus:@"Loading SubSheets..."];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
    [self initialze];
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
}
- (void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];

}

#pragma mark- -LeaseManager
- (void) initialze{
    
    DZSubSheetsManager *subSheetsManager = [[DZSubSheetsManager alloc]init];
    subSheetsManager.delegate = self;
    subSheetsManager.sheetsID = self.sheetsID;

    [subSheetsManager requestSubSheets];
}

#pragma mark- -SubSheetsManagerDelegate

- (void) subSheetsManager:(DZSubSheetsManager *)manager didFailToReceiveSubSheets:(NSError *)error{
    [self showErrorAlert:error];
}

- (void) subSheetsManager:(DZSubSheetsManager *)manager didReceiveSubSheets:(NSArray *)subSheets error:(NSError*)error{
    [self.refreshControl endRefreshing];
    if (error !=nil) {
        [self showErrorAlert:error];
    }else
    {
        [self.refreshControl endRefreshing];
        self.subSheets = subSheets;
        [self.tableView reloadData];
    }
}

- (void)showErrorAlert:(NSError*)error{
    
    [ZAActivityBar dismiss];
    [self.refreshControl endRefreshing];

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Operation Failed" message:error.localizedDescription delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again", nil];
    
    [alert show];
}

#pragma mark- -TableViewDaraSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.subSheets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    
    DZSubSheetsModel *subSheetModel = [self.subSheets objectAtIndex:indexPath.row];
    
	NSString *cellName = subSheetModel.name;
    
	cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = cellName;
    
   // cell.textLabel.textAlignment = NSTextAlignmentCenter;
    [[cell textLabel] setFont:[UIFont fontWithName:kAppFont size: 20.0]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        [ZAActivityBar dismiss];
           [self.navigationItem setHidesBackButton:NO animated:NO];
           self.navigationItem.titleView.center = self.navigationController.navigationBar.center;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    return view;
}

#pragma mark- -TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DZSubSheetsModel *subSheetModel = [self.subSheets objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"subSheetsToFieldsView" sender:[NSNumber numberWithInteger:subSheetModel.subSheetsID]];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DZFieldsVC *fieldView = [segue destinationViewController];
    fieldView.subSheetID = [sender integerValue];
    fieldView.isSubSheetField = YES;
    fieldView.leaseID = self.leaseID;
    fieldView.sheetID = self.sheetsID;
}
#pragma mark- -UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSLog(@"button index: %d",buttonIndex);
    if(buttonIndex == 1){
        [self initialze];
    }
    else if (buttonIndex ==0)
        [ZAActivityBar dismiss];
}


@end
