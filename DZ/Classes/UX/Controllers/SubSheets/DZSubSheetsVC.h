//
//  DZSubSheetViewController.h
//  DZ
//
//  Created by Ikarma Khan on 30/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DZSubSheetsVC : UITableViewController<UIAlertViewDelegate>


@property (nonatomic)NSInteger sheetsID;
@property (nonatomic)NSInteger leaseID;
@end
