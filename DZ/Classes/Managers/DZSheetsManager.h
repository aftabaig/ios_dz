//
//  DZSheetsManager.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DZSheetsManager;
@protocol DZSheetsManagerDelegate

@optional
- (void)sheetManager:(DZSheetsManager*)manager didReceiveSheets:(NSArray*)sheets error:(NSError*)error;;
- (void)sheetManager:(DZSheetsManager*)manager didFailToReceiveSheets:(NSError*)error;
@end


@interface DZSheetsManager : NSObject

@property(weak,nonatomic)id<DZSheetsManagerDelegate>delegate;
@property(nonatomic) NSInteger leaseID;

- (void) requestSheets;

@end