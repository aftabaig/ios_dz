//
//  DZFieldsManager.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>



@class DZFieldsManager;
@protocol DZFieldManagerDelegate

@optional
- (void)fieldManager:(DZFieldsManager*)manager didReceiveFields:(NSArray*)fields error:(NSError*)error;;
- (void)fieldManager:(DZFieldsManager*)manager didFailReceiveFields:(NSError*)error;
@end


@interface DZFieldsManager : NSObject


@property(weak,nonatomic)id<DZFieldManagerDelegate>delegate;

@property (nonatomic)NSInteger leaseID;

@property (nonatomic)NSInteger sheetID;

@property(nonatomic) BOOL isSubSheetField;
@property (nonatomic)NSInteger subSheetID;


@property (nonatomic) NSInteger month;
@property (nonatomic) NSInteger year;

- (void) requestFields;

@end
