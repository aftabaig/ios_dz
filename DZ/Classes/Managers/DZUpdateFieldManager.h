//
//  DZUpdateFieldManager.h
//  DZ
//
//  Created by Ikarma Khan on 15/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>


@class DZUpdateFieldManager;
@protocol DZUpdateFieldManagerDelegate

@optional
- (void)updateFieldManager:(DZUpdateFieldManager*)updateFieldManager didUpdateFields:(BOOL)status error:(NSError*)error;
- (void)updateFieldManager:(DZUpdateFieldManager*)didFailToUpdateFieldManager :(NSError*)error;
@end


@interface DZUpdateFieldManager : NSObject

@property (weak,nonatomic) id<DZUpdateFieldManagerDelegate>delegate;
@property (strong,nonatomic) NSArray *updatedFields;

- (void)postFields;

@end
