//
//  DZNetworkEngine.m
//  DZ
//
//  Created by Ikarma Khan on 16/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZNetworkEngine.h"

@implementation DZNetworkEngine
+ (id)sharedInstance
{
    static DZNetworkEngine *sharedInstance;
    static dispatch_once_t done;
    
    dispatch_once(&done, ^{
        sharedInstance = [[DZNetworkEngine alloc] initWithHostName:API_BASE_URL];
        [sharedInstance useCache];
    });
    
    return sharedInstance;
}


@end
