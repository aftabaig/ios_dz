//
//  DZLeaseManager.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DZLeasesManager;
@protocol DZLeasesManagerDelegate

@optional
- (void)leaseManager:(DZLeasesManager*)manager didReceiveLeases:(NSArray*)leases error:(NSError*)error;
- (void)leaseManager:(DZLeasesManager*)manager didFailToReceiveLeases:(NSError*)error;
@end


@interface DZLeasesManager : NSObject

@property(weak,nonatomic)id<DZLeasesManagerDelegate>delegate;

- (void) requestLeases;

@end
