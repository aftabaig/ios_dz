//
//  DZSubSheetsManager.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZSubSheetsManager.h"
#import "DZSubSheetsModel.h"
#import "DZNetworkEngine.h"

@implementation DZSubSheetsManager


- (void)requestSubSheets{
    

    DZNetworkEngine *networkEngine = [DZNetworkEngine sharedInstance];
    MKNetworkOperation *operation = [networkEngine operationWithPath:[NSString stringWithFormat:API_SUBSHEETS_URL,self.sheetsID]
                                                              params:nil
                                                          httpMethod:@"GET"];
    
    [operation addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSMutableArray *subSheets  = [[NSMutableArray alloc]init];
        
        NSArray *arrSubSheets = completedOperation.responseJSON;
        for(NSDictionary *dictSubSheet in arrSubSheets){
            
            DZSubSheetsModel *subSheetModel = [[DZSubSheetsModel alloc]initWithDictionary:dictSubSheet];
            [subSheets addObject:subSheetModel];
        }
        if(self.delegate){
            [self.delegate subSheetsManager:self didReceiveSubSheets:subSheets error:completedOperation.error];
        }
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        if (self.delegate)
        {
          [self.delegate subSheetsManager:self didFailToReceiveSubSheets:error];
        }
        
    }];
    
    [networkEngine enqueueOperation:operation forceReload:YES];
}


@end
