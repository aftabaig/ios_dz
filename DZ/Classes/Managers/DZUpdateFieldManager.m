//
//  DZUpdateFieldManager.m
//  DZ
//
//  Created by Ikarma Khan on 15/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZUpdateFieldManager.h"
#import "DZNetworkEngine.h"
#import "JSON.h"
@implementation DZUpdateFieldManager

- (void)postFields{

    DZNetworkEngine *networkEngine = [DZNetworkEngine sharedInstance];
    
    MKNetworkOperation *operation = [networkEngine operationWithPath:API_UPDATE_FIELDS_URL
                                                                     params:nil
                                                                 httpMethod:@"POST"];
    
    

    operation.postDataEncoding = MKNKPostDataEncodingTypeJSON;
    [operation setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {

        return self.updatedFields.JSONRepresentation;
    } forType:@"application/json"];
    
    [operation addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        NSDictionary *dictResponse = completedOperation.responseJSON;
        NSLog(@"%@",dictResponse);
        BOOL success = (completedOperation.error == NULL)? TRUE : FALSE;
        [self.delegate updateFieldManager:self didUpdateFields:success error:completedOperation.error];
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        if (self.delegate)
        {
          [self.delegate updateFieldManager:self :error];
        }
        
    }];
    
    [networkEngine enqueueOperation:operation forceReload:YES];

    
}

@end
