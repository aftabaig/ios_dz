//
//  APIConstants.h
//  DZ
//
//  Created by Ikarma Khan on 31/07/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#ifndef DZ_APIConstants_h
#define DZ_APIConstants_h

#define API_BASE_URL                @"dz-explore.herokuapp.com"

#define API_LEASES_URL              @"api/leases"

#define API_SHEETS_URL              @"api/leases/%d/sheets"

#define API_SUBSHEETS_URL           @"api/sheets/%d/subSheets"

#define API_FIELDS_SHEETS_URL       @"api/data/leases/%d/sheets/%d/month/%d/year/%d/"

#define API_FIELDS_SUBSHEETS_URL    @"api/data/leases/%d/subsheets/%d/month/%d/year/%d/"

#define API_UPDATE_FIELDS_URL       @"api/data/"

#define API_AUTHENTICATE_USER       @""



#endif
