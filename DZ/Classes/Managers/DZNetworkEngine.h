//
//  DZNetworkEngine.h
//  DZ
//
//  Created by Ikarma Khan on 16/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//


#import "MKNetworkEngine.h"
@interface DZNetworkEngine : MKNetworkEngine

+ (id)sharedInstance;
@end

