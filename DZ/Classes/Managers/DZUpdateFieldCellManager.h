//
//  DZUpdateFieldCellManager.h
//  DZ
//
//  Created by Ikarma Khan on 20/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DZUpdateFieldCellManager;

@protocol DZUpdateCellManagerDelegate

@required
- (void)updateFieldValue:(DZUpdateFieldCellManager*)updatedCell updateValue:(NSString*)updatedValue;
@end

@interface DZUpdateFieldCellManager : NSObject

@property (weak,nonatomic) id<DZUpdateCellManagerDelegate>delegate;

@property (strong,nonatomic) NSDictionary* fieldCellValues;

- (void) initWithDictionary: (NSDictionary*)fieldInfo;
@end

