//
//  DZFieldsManager.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZFieldsManager.h"
#import "DZFieldsModel.h"
#import "DZNetworkEngine.h"

@implementation DZFieldsManager

- (void) requestFields{
    
    
    NSString *url;
    if(self.isSubSheetField) {
     
        url = [NSString stringWithFormat:API_FIELDS_SUBSHEETS_URL,
                      self.leaseID,self.subSheetID,self.month,self.year];
        
    }
    else{
       
        // urlFields = [NSURL URLWithString:[NSString stringWithFormat:@"%@/sheets/%d/fields",kBaseURL,self.sheetID]];
        
        url=[NSString stringWithFormat:API_FIELDS_SHEETS_URL,
                                          self.leaseID,self.sheetID,self.month,self.year];
        
        
        
//        http://dz-explore.herokuapp.com/api/data/leases/1/sheets/1/month/5/year/2014/
    }
    
    
    
    DZNetworkEngine *networkEngine = [DZNetworkEngine sharedInstance];
    MKNetworkOperation *operation = [networkEngine operationWithPath:url
                                                              params:nil
                                                          httpMethod:@"GET"];
    
    [operation addCompletionHandler:^(MKNetworkOperation *completedOperation) {

        NSMutableArray *fields  = [[NSMutableArray alloc]init];
        NSArray *arrFields = completedOperation.responseJSON;
        
        for(NSDictionary *dictField in arrFields){
            
            DZFieldsModel *fieldModel = [[DZFieldsModel alloc]initWithDictionary:dictField];
            [fields addObject:fieldModel];
        }
        
        if(self.delegate){
            [self.delegate fieldManager:self didReceiveFields:fields error:completedOperation.error];
        }
 
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {

        if (self.delegate)
        {
            [self.delegate fieldManager:self didFailReceiveFields:error];
        }
        
    }];

    [networkEngine enqueueOperation:operation forceReload:YES];
    
       
}
@end
