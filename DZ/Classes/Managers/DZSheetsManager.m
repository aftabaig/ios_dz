//
//  DZSheetsManager.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZSheetsManager.h"
#import "DZSheetsModel.h"
#import "DZNetworkEngine.h"

@implementation DZSheetsManager

- (void) requestSheets{
    
    DZNetworkEngine *networkEngine = [DZNetworkEngine sharedInstance];
    MKNetworkOperation *operation = [networkEngine operationWithPath:[NSString stringWithFormat:API_SHEETS_URL,self.leaseID]
                                                              params:nil
                                                          httpMethod:@"GET"];
    
    [operation addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSMutableArray *sheets = [[NSMutableArray alloc]init];
        NSArray* arrSheets = completedOperation.responseJSON;

        for(NSDictionary *dictSheet in arrSheets){
            
            DZSheetsModel *sheetModel = [[DZSheetsModel alloc]initWithDictionary:dictSheet];
            [sheets addObject:sheetModel];
        }
        
        if(self.delegate){
            [self.delegate sheetManager:self didReceiveSheets:sheets error:completedOperation.error];
        }
        
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        if (self.delegate)
        {
            [self.delegate sheetManager:self didFailToReceiveSheets:error];
        }
        
    }];
    
    [networkEngine enqueueOperation:operation forceReload:YES];
}

@end
