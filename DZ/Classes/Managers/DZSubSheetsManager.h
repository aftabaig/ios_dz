//
//  DZSubSheetsManager.h
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DZSubSheetsManager;

@protocol DZSubSheetsDelegate

@optional

- (void) subSheetsManager:(DZSubSheetsManager*)manager didReceiveSubSheets:(NSArray*)subSheets error:(NSError*)error;;
- (void) subSheetsManager:(DZSubSheetsManager*)manager didFailToReceiveSubSheets:(NSError*)error;
@end


@interface DZSubSheetsManager : NSObject

@property (weak,nonatomic)id<DZSubSheetsDelegate> delegate;

@property (nonatomic) NSInteger sheetsID;

- (void) requestSubSheets;

@end
