//
//  DZLogInManager.h
//  DZ
//
//  Created by Ikarma Khan on 23/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <Foundation/Foundation.h>


@class DZLogInManager;
@protocol DZLogInManagerDelegate

@optional
- (void)loginManager:(DZLogInManager*)manager didFailToLoginWithError:(NSError*)error;
- (void)loginManager:(DZLogInManager*)manager didRecieveLogInStatus: (BOOL)logInStats error:(NSError*)error;

@end

@interface DZLogInManager : NSObject

@property(nonatomic,strong) id<DZLogInManagerDelegate>delegate;

@property (nonatomic,strong) NSString *userName;
@property (nonatomic,strong) NSString *password;

- (void)logUserIn;

@end
