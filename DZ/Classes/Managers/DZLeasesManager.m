//
//  DZLeaseManager.m
//  DZ
//
//  Created by Ikarma Khan on 04/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZLeasesManager.h"
#import "DZLeasesModel.h"
#import "DZNetworkEngine.h"

@implementation DZLeasesManager


- (void) requestLeases{
    
    DZNetworkEngine *networkEngine = [DZNetworkEngine sharedInstance];
    MKNetworkOperation *operation = [networkEngine operationWithPath:API_LEASES_URL
                                                              params:nil
                                                          httpMethod:@"GET"];
    
    
    [operation addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSMutableArray *leases = [[NSMutableArray alloc]init];
        
        NSArray* arrLeases = completedOperation.responseJSON;
        for(NSDictionary *dictLease in arrLeases){
            
            DZLeasesModel *leaseModel = [[DZLeasesModel alloc]initWithDictionary:dictLease];
            [leases addObject:leaseModel];
        }
        
        if(self.delegate){
            
            
            [self.delegate leaseManager:self didReceiveLeases:leases error:completedOperation.error];
        }
        

        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        if (self.delegate)
        {
            [self.delegate leaseManager:self didFailToReceiveLeases:error];
        }
        
    }];
    
    [networkEngine enqueueOperation:operation forceReload:YES];
    
}
@end
