//
//  DZLogInManager.m
//  DZ
//
//  Created by Ikarma Khan on 23/05/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import "DZLogInManager.h"
#import "DZLogInManager.h"

#import "DZNetworkEngine.h"
#import "JSON.h"
@implementation DZLogInManager

- (void)logUserIn{
    
    DZNetworkEngine *networkEngine = [DZNetworkEngine sharedInstance];
    MKNetworkOperation *operation = [networkEngine operationWithPath:@""
                                                              params:nil
                                                          httpMethod:@"POST" ];
    
    operation.postDataEncoding = MKNKPostDataEncodingTypeJSON;
    [operation setCustomPostDataEncodingHandler:^NSString*(NSDictionary* postData) {

        return [[NSArray alloc]initWithObjects:self.userName,self.password, nil].JSONRepresentation;
    
    }forType:@"application/json"];
    
    [operation addCompletionHandler:^(MKNetworkOperation* completedOperation){
        if (self.delegate) {
            [self.delegate loginManager:self didRecieveLogInStatus:YES error:completedOperation.error];
        }
    }errorHandler:^(MKNetworkOperation *completedOperation, NSError* error){
        
        if(self.delegate)
            [self.delegate loginManager:self didFailToLoginWithError:error];
        
    }];
    
    [networkEngine enqueueOperation:operation forceReload:YES];
}

@end
