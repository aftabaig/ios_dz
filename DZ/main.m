//
//  main.m
//  DZ
//
//  Created by Ikarma Khan on 28/04/2014.
//  Copyright (c) 2014 iKarmaKhan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DZAppDelegate class]));
    }
}
